﻿using GarlicSpriteEditor.Scripts.Scenes;
using GarlicSpriteEditor.Scripts.Utils;
using GarlicSpriteEditor.Scripts.Utils.ResourceHolder;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Monocle;
using MonoGarlic.Utils;
using System.Collections.Generic;

namespace GarlicSpriteEditor
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class SpriteEditorMain : Engine
	{
		ContentHolder content_loader;
		public SpriteEditorMain(int width, int height, int windowedScale, string windowTitle, bool fullscreen, Monocle_Platform platform)
			: base(width, height, windowedScale, windowTitle, fullscreen, platform)
		{
			content_loader = new ContentHolder();
			ClearColor = Color.CornflowerBlue;
			this.IsMouseVisible = true;
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			//Initialize -> LoadContent -> LoadScene
			base.Initialize();
			LoadScene();
		}

		
		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			base.LoadContent();
			content_loader.Load();
			GUIDefaults.Load();
		}

		void LoadScene()
		{
			SpriteEditorMainScene currentScene = new SpriteEditorMainScene();
			Scene = currentScene;
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
			base.UnloadContent();
		}
	}
}
