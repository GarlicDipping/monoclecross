﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Entities
{
	public class ImageEntity : Entity
	{
		public Image component_image { get; protected set; }
		public ImageEntity(MTexture tex) : base()
		{
			component_image = new Image(tex);
			Add(component_image);
		}		
	}
}
