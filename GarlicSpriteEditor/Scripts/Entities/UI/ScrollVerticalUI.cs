﻿using GarlicSpriteEditor.Scripts.Components;
using GarlicSpriteEditor.Scripts.Utils;
using GarlicSpriteEditor.Scripts.Utils.ResourceHolder;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GarlicSpriteEditor.Scripts.Entities.UI
{
	public class ScrollVerticalUI : Entity
	{
		Vector2 arrow_up_pos, scrollbar_bg_pos, arrow_down_pos;
		Vector2 arrow_up_size, scrollbar_bg_size, arrow_down_size;

		SimpleButtonUI arrow_up, arrow_down;
		NinePatchImage scroll_bg;
		public ScrollVerticalUI(Vector2 position, Vector2 size) : base()
		{
			setup_position(position, size);
			setup_size(position, size);

			arrow_up = new SimpleButtonUI(GUIDefaults.scroll_arrow_up_normal, GUIDefaults.scroll_arrow_up_pressed,
				arrow_up_pos, arrow_up_size, GUIDefaults.Scrollbar_arrow_up_xml);
			
			arrow_down = new SimpleButtonUI(GUIDefaults.scroll_arrow_down_normal, GUIDefaults.scroll_arrow_down_pressed,
				arrow_down_pos, arrow_down_size, 
				GUIDefaults.Scrollbar_arrow_down_xml);

			scroll_bg = new NinePatchImage(GUIDefaults.scroll_bg, GUIDefaults.ScrollbarBackground_xml,
				scrollbar_bg_size);
			scroll_bg.Position = scrollbar_bg_pos;

			Add(scroll_bg);
		}

		private void setup_position(Vector2 original_pos, Vector2 original_size)
		{
			float arrow_img_height = GUIDefaults.scroll_arrow_down_normal.Height;
			float scroll_bg_size = original_size.Y - arrow_img_height * 2f;

			arrow_up_pos = original_pos;
			scrollbar_bg_pos = new Vector2(original_pos.X, original_pos.Y + arrow_img_height);
			arrow_down_pos = new Vector2(original_pos.X, original_pos.Y + arrow_img_height + scroll_bg_size);
		}

		private void setup_size(Vector2 original_pos, Vector2 original_size)
		{
			float arrow_img_height = GUIDefaults.scroll_arrow_down_normal.Height;
			arrow_up_size = arrow_down_size = new Vector2(original_size.X, arrow_img_height);
			scrollbar_bg_size = new Vector2(original_size.X, original_size.Y - arrow_img_height * 2f);
		}

		public override void Added(Scene scene)
		{
			base.Added(scene);
			Scene.Add(arrow_up);
			Scene.Add(arrow_down);
		}

		public override void Render()
		{
			base.Render();
		}
	}
}
