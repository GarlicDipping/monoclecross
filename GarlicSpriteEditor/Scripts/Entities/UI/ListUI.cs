﻿using GarlicSpriteEditor.Scripts.Entities.UI.Interfaces;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Entities.UI
{
	public class ListUI : Entity
	{
		Vector2 size;
		List<ListUIItems> list_items;
		public ListUI(Vector2 size) : base()
		{
			this.size = size;
		}

		public void set_items(List<ListUIItems> list_items)
		{
			this.list_items = list_items;
		}

		public override void Update()
		{
			base.Update();
			foreach(ListUIItems item in list_items)
			{
				item.Render();
			}
		}

		public override void Render()
		{
			base.Render();
		}
	}

	public abstract class ListUIItems : Entity, IMouseHoverInterface
	{
		/// <summary>
		/// 이 리스트 아이템이 홀드중인 데이터
		/// </summary>
		public object Data;
		public bool clickable = true;

		ListUI parent;
		Color bg_color;
		public ListUIItems(ListUI parent, object Data)
		{
			this.parent = parent;
			this.Data = Data;
			bg_color = new Color(0, 0, 0, 0.2f);
		}

		public override void Render()
		{
			base.Render();
			//Monocle.Draw.Rect(parent)
		}

		public void OnMouseHover(Entity entity)
		{
			
		}

		public void OnMouseHoverEnter(Entity entity)
		{
			throw new NotImplementedException();
		}

		public void OnMouseHoverExit(Entity entity)
		{
			throw new NotImplementedException();
		}
	}
}
