﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Entities.UI.Interfaces
{
	public interface IMouseHoverInterface
	{
		void OnMouseHoverEnter(Entity entity);
		void OnMouseHover(Entity entity);
		void OnMouseHoverExit(Entity entity);
	}
}
