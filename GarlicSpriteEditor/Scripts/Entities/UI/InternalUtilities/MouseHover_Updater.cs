﻿using GarlicSpriteEditor.Scripts.Entities.UI.Interfaces;
using GarlicSpriteEditor.Scripts.Utils;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Entities.UI.InternalUtilities
{
	public class MouseHover_Updater
	{
		Dictionary<Entity, bool> mouse_hovered_before_dict;

		public MouseHover_Updater()
		{
			mouse_hovered_before_dict = new Dictionary<Entity, bool>();
		}

		/// <summary>
		/// Update All Entities from scene.
		/// IMouseHoverInterface를 구현하는지 여부는 여기서 알아서 처리함
		/// </summary>
		/// <param name="all_entities"></param>
		public void Update_Mouse_Hover(List<Entity> all_entities)
		{
			List<Entity> hoverable_entities = InterfaceFetcher.fecth_entities<IMouseHoverInterface>(all_entities);
			update_dict(hoverable_entities);

			foreach (Entity e in hoverable_entities)
			{
				IMouseHoverInterface i = (IMouseHoverInterface)e;
				bool hovered = mouse_hovered_before_dict[e];

				if (e.Collider.Collide(new Vector2(MInput.Mouse.X, MInput.Mouse.Y)))
				{
					if (hovered == false)
					{
						i.OnMouseHoverEnter(e);
					}
					else
					{
						i.OnMouseHover(e);
					}
					mouse_hovered_before_dict[e] = true;
				}
				else
				{
					if (hovered == true)
					{
						i.OnMouseHoverExit(e);
					}
					mouse_hovered_before_dict[e] = false;
				}
			}
		}

		void update_dict(List<Entity> target_entities)
		{
			foreach (Entity e in mouse_hovered_before_dict.Keys)
			{
				if (target_entities.Contains(e) == false)
				{
					mouse_hovered_before_dict.Remove(e);
				}
			}

			foreach (Entity e in target_entities)
			{
				if (mouse_hovered_before_dict.ContainsKey(e) == false)
				{
					mouse_hovered_before_dict.Add(e, false);
				}
			}
		}
	}
}
