﻿using GarlicSpriteEditor.Scripts.Components;
using GarlicSpriteEditor.Scripts.Entities.UI.Interfaces;
using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GarlicSpriteEditor.Scripts.Entities.UI
{
	public class SimpleButtonUI : Entity, IMouseHoverInterface
	{
		public Action onClicked;
		
		MTexture normal_texture, pressed_texture;
		NinePatchImage current_image;
		public SimpleButtonUI(MTexture normal, MTexture pressed, XmlDocument nine_patch_meta = null)
			: base()
		{
			normal_texture = normal;
			pressed_texture = pressed;
			current_image = nine_patch_meta == null ? new NinePatchImage(normal) : 
				new NinePatchImage(normal, nine_patch_meta);
			Add(current_image);

			//Add Clickable Bound
			Collider = new Hitbox((int)normal.Width, (int)pressed.Height, 0, 0);
		}

		public SimpleButtonUI(MTexture normal, MTexture pressed, Vector2 pos, XmlDocument nine_patch_meta = null)
			: this(normal, pressed, nine_patch_meta)
		{
			Position = pos;
			current_image.Position = pos;
		}

		public SimpleButtonUI(MTexture normal, MTexture pressed, Vector2 pos, Vector2 size,
			XmlDocument nine_patch_meta = null)
			: this(normal, pressed, nine_patch_meta)
		{
			current_image.setSize(size);
			Position = pos;
			current_image.Position = pos;
			Collider.Width = size.X;
			Collider.Height = size.Y;
		}

		bool draw_outline;
		public override void Update()
		{
			Check_Button_Click();
		}

		void Check_Button_Click()
		{
			if(Collider.Collide(new Vector2(MInput.Mouse.X, MInput.Mouse.Y)))
			{
				if(MInput.Mouse.PressedLeftButton)
				{
					if (onClicked != null)
					{
						onClicked.Invoke();
					}
					set_image(pressed_texture);
				}
			}

			if(MInput.Mouse.ReleasedLeftButton)
			{
				set_image(normal_texture);
			}
		}

		public override void Render()
		{
			if(current_image != null && draw_outline)
			{
				current_image.DrawOutline();
			}
			base.Render();
		}

		void set_bound(Rectangle rect)
		{
			((Hitbox)Collider).SetFromRectangle(rect);
		}

		void set_bound(int x, int y, int width, int height)
		{
			set_bound(new Rectangle(x, y, width, height));
		}

		void set_image(MTexture texture)
		{
			if(current_image != null && 
				current_image.Texture == texture)
			{
				return;
			}
			else
			{
				current_image.Texture = texture;
			}
		}

		#region Mouse Hover Callback

		public void OnMouseHoverEnter(Entity entity)
		{
			draw_outline = true;
		}

		public void OnMouseHover(Entity entity)
		{
		}

		public void OnMouseHoverExit(Entity entity)
		{
			draw_outline = false;
		}

		#endregion
	}
}
