﻿using GarlicSpriteEditor.Scripts.Entities;
using GarlicSpriteEditor.Scripts.Entities.UI;
using GarlicSpriteEditor.Scripts.Entities.UI.Interfaces;
using GarlicSpriteEditor.Scripts.Entities.UI.InternalUtilities;
using GarlicSpriteEditor.Scripts.Utils;
using GarlicSpriteEditor.Scripts.Utils.ResourceHolder;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GarlicSpriteEditor.Scripts.Scenes
{
	public class SpriteEditorMainScene : Scene
	{
		BitTag TAG_UI = new BitTag("EDITOR_UI");

		MouseHover_Updater mousehover_updater;

		/// <summary>
		/// Renders UI Only
		/// </summary>
		SingleTagRenderer EditorUIRenderer;
		/// <summary>
		/// Renders Other sprites / ...
		/// </summary>
		TagExcludeRenderer EditorMainRenderer;

		List<Entity> ui_entities;
		/// <summary>
		/// Atlases to edit
		/// </summary>
		List<ImageEntity> atlas_image_list;
		public SpriteEditorMainScene() : base()
		{
			mousehover_updater = new MouseHover_Updater();
			ui_entities = new List<Entity>();
			atlas_image_list = new List<ImageEntity>();

			add_entities();
		}

		void add_entities()
		{
			add_renderers();
			add_UIs();
			add_atlases();


			//XmlDocument nine_patch_xml = Calc.LoadContentXML("XmlDatas/NinePatchTest.xml");
			//NinePatchEntity np = new NinePatchEntity(ContentHolder.Instance.Editor_UI_Dict["ScrollbarBackground"],
			//	nine_patch_xml, new Vector2(16, 64));
			//np.AddTag(TAG_UI);
			ScrollVerticalUI scroll = new ScrollVerticalUI(Vector2.One * 100, new Vector2(18, 64));
			scroll.AddTag(TAG_UI);

			Add(scroll);
		}

		void add_renderers()
		{
			EditorUIRenderer = new SingleTagRenderer(TAG_UI);
			EditorMainRenderer = new TagExcludeRenderer(TAG_UI);
			Add(EditorUIRenderer);
			Add(EditorMainRenderer);
		}

		SimpleButtonUI btn;
		void add_UIs()
		{
			btn = new SimpleButtonUI(
				GUIDefaults.handle_left_normal, GUIDefaults.handle_left_pressed,
				new Vector2(0, Engine.Height / 2f));
			btn.AddTag(TAG_UI);
			Add(btn);
		}

		void add_atlases()
		{
			foreach (var kv in ContentHolder.Instance.Atlas_Dict)
			{
				ImageEntity img = new ImageEntity(kv.Value);
				atlas_image_list.Add(img);
				Add(img);
			}
		}

		public override void Begin()
		{
			base.Begin();
		}
		
		public override void Update()
		{
			base.Update();
			mousehover_updater.Update_Mouse_Hover(Entities.ToList());

			if(MInput.Keyboard.Check(Keys.Right))
			{
				EditorMainRenderer.Camera.X += 1;
			}
			else if (MInput.Keyboard.Check(Keys.Left))
			{
				EditorMainRenderer.Camera.X -= 1;
			}
			
		}

		public override void Render()
		{
			base.Render();
			debug_mouse();
		}

		void debug_mouse()
		{
			Draw.SpriteBatch.Begin();
			Monocle.Draw.Text(Monocle.Draw.DefaultFont, "Mouse Pos : " + MInput.Mouse.Position, new Vector2(Engine.Width, 0), Color.Black);
			Monocle.Draw.Text(Monocle.Draw.DefaultFont, "Button Rect : (" + btn.Collider.AbsoluteLeft + ", " + btn.Collider.AbsoluteTop + ", " 
				+ btn.Collider.Width + ", " + btn.Collider.Height + ")", new Vector2(Engine.Width, 20), Color.Black);

			Draw.SpriteBatch.End();
		}
	}
}
