﻿using Microsoft.Xna.Framework;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GarlicSpriteEditor.Scripts.Components
{
	public class NinePatchImage : Image
	{
		Rectangle[] sourceRects;
		XmlDocument meta;

		public NinePatchImage(MTexture texture) : base(texture)
		{
			LeftPadding = RightPadding = TopPadding = BottomPadding = 0;
		}

		public bool isNinePatchImage
		{
			get { return LeftPadding != 0 || RightPadding != 0 || TopPadding != 0 || BottomPadding != 0; }
		}

		public NinePatchImage(MTexture texture, XmlDocument meta) : base(texture)//, XmlDocument ninepatch_xml) : base(texture)
		{
			this.meta = meta;
			parseNinepatchXml();
			sourceRects = CreatePatches();
		}

		public NinePatchImage(MTexture texture, XmlDocument meta, Vector2 size) : this(texture, meta)//, XmlDocument ninepatch_xml) : base(texture)
		{
			this.meta = meta;
			parseNinepatchXml();
			setSize(size);
		}
		
		/// <summary>
		/// 실제 텍스쳐 사이즈와는 관계없이, 월드에서 보이게 될 Width & Height
		/// </summary>
		public override float Width
		{
			get
			{
				return Scale.X * Texture.Width;
			}
		}

		public override float Height
		{
			get
			{
				return Scale.Y * Texture.Height;
			}
		}

		private Rectangle[] CreatePatches()
		{
			Rectangle rectangle = new Rectangle((int)X, (int)Y, (int)Width, (int)Height);
			var x = rectangle.X;
			var y = rectangle.Y;
			var w = rectangle.Width;
			var h = rectangle.Height;
			var middleWidth = w - LeftPadding - RightPadding;
			var middleHeight = h - TopPadding - BottomPadding;
			var bottomY = y + h - BottomPadding;
			var rightX = x + w - RightPadding;
			var leftX = x + LeftPadding;
			var topY = y + TopPadding;
			var patches = new[]
			{
				new Rectangle(x,      y,        LeftPadding,  TopPadding),      // top left
				new Rectangle(leftX,  y,        middleWidth,  TopPadding),      // top middle
				new Rectangle(rightX, y,        RightPadding, TopPadding),      // top right
				new Rectangle(x,      topY,     LeftPadding,  middleHeight),    // left middle
				new Rectangle(leftX,  topY,     middleWidth,  middleHeight),    // middle
				new Rectangle(rightX, topY,     RightPadding, middleHeight),    // right middle
				new Rectangle(x,      bottomY,  LeftPadding,  BottomPadding),   // bottom left
				new Rectangle(leftX,  bottomY,  middleWidth,  BottomPadding),   // bottom middle
				new Rectangle(rightX, bottomY,  RightPadding, BottomPadding)    // bottom right
			};
			return patches;
		}

		public override void Render()
		{
			if(isNinePatchImage)
			{
				renderNinePatch();
			}
			else
			{
				base.Render();
			}
		}

		void renderNinePatch()
		{
			var destinationPatches = CreatePatches();

			for (var i = 0; i < sourceRects.Length; i++)
			{
				Monocle.Draw.SpriteBatch.Draw(Texture.Texture2D, sourceRectangle: sourceRects[i],
					destinationRectangle: destinationPatches[i], color: Color);
			}
		}

		int LeftPadding, RightPadding, TopPadding, BottomPadding;
		private void parseNinepatchXml()
		{
			XmlElement at = meta["GarlicNinePatch"];

			LeftPadding = at["LeftPadding"].InnerInt();
			RightPadding = at["RightPadding"].InnerInt();
			TopPadding = at["TopPadding"].InnerInt();
			BottomPadding = at["BottomPadding"].InnerInt();
		}
	}
}
