﻿using GarlicSpriteEditor.Scripts.Components;
using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace GarlicSpriteEditor.Scripts.Utils.ResourceHolder
{
	public static class GUIDefaults
	{
		public static MTexture handle_left_normal, handle_left_pressed;

		public static MTexture scroll_arrow_up_normal, scroll_arrow_up_pressed,
			scroll_arrow_down_normal, scroll_arrow_down_pressed, scroll_bg;
		public static XmlDocument Scrollbar_arrow_down_xml, Scrollbar_arrow_up_xml, ScrollbarBackground_xml;

		public static void Load()
		{
			LoadHandleResources();
			LoadScrollbarResources();			
		}

		static void LoadHandleResources()
		{
			handle_left_normal = ContentHolder.Instance.Editor_UI_Dict["Handle_Left_Normal"];
			handle_left_pressed = ContentHolder.Instance.Editor_UI_Dict["Handle_Left_Pressed"];
		}

		static void LoadScrollbarResources()
		{
			Scrollbar_arrow_down_xml = Calc.LoadContentXML("XmlDatas/Scrollbar_arrow_down.xml");
			Scrollbar_arrow_up_xml = Calc.LoadContentXML("XmlDatas/Scrollbar_arrow_up.xml");
			ScrollbarBackground_xml = Calc.LoadContentXML("XmlDatas/ScrollbarBackground.xml");
			scroll_arrow_up_normal = ContentHolder.Instance.Editor_UI_Dict["Scrollbar_arrow_up_normal"];
			scroll_arrow_up_pressed = ContentHolder.Instance.Editor_UI_Dict["Scrollbar_arrow_up_pressed"];
			scroll_arrow_down_normal = ContentHolder.Instance.Editor_UI_Dict["Scrollbar_arrow_down_normal"];
			scroll_arrow_down_pressed = ContentHolder.Instance.Editor_UI_Dict["Scrollbar_arrow_down_pressed"];

			scroll_bg = ContentHolder.Instance.Editor_UI_Dict["ScrollbarBackground"];
		}
	}
}
