﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Utils
{
	public class InterfaceFetcher
	{
		public static List<Entity> fecth_entities<T>(List<Entity> from)
		{
			List<Entity> entities_implement_interface = new List<Entity>();
			for(int i = 0; i < from.Count; i++)
			{
				if(from[i] is T)
				{
					entities_implement_interface.Add(from[i]);
				}
			}
			return entities_implement_interface;
		}
	}
}
