﻿using Microsoft.Xna.Framework.Content;
using Monocle;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MonoGarlic.Utils
{
    public static class ContentLoader
    {
		public static Dictionary<string, MTexture> LoadMTextures(this ContentManager contentManager, string contentFolder)
		{
			DirectoryInfo dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentFolder);
			if (!dir.Exists)
				throw new DirectoryNotFoundException();
			Dictionary<string, MTexture> result = new Dictionary<string, MTexture>();

			FileInfo[] files = dir.GetFiles("*.*");
			foreach (FileInfo file in files)
			{
				string key = Path.GetFileNameWithoutExtension(file.Name);
				result[key] = new MTexture(contentFolder + "/" + key);
			}
			return result;
		}

		public static Dictionary<string, T> LoadListContent<T>(this ContentManager contentManager, string contentFolder)
		{
			DirectoryInfo dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentFolder);
			if (!dir.Exists)
				throw new DirectoryNotFoundException();
			Dictionary<String, T> result = new Dictionary<String, T>();

			FileInfo[] files = dir.GetFiles("*.*");
			foreach (FileInfo file in files)
			{
				string key = Path.GetFileNameWithoutExtension(file.Name);
				result[key] = contentManager.Load<T>(contentFolder + "/" + key);
			}
			return result;
		}
	}
}
