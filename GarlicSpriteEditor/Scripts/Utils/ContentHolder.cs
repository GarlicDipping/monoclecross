﻿using Microsoft.Xna.Framework.Graphics;
using Monocle;
using MonoGarlic.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GarlicSpriteEditor.Scripts.Utils
{
	public class ContentHolder
	{
		public static ContentHolder Instance;
		public Dictionary<string, MTexture> Atlas_Dict;
		public Dictionary<string, MTexture> Editor_UI_Dict;

		public ContentHolder()
		{
			Instance = this;
		}

		public void Load()
		{
			Load_Atlases();
		}

		void Load_Atlases()
		{
			Atlas_Dict = ContentLoader.LoadMTextures(SpriteEditorMain.Instance.Content, "Atlas");
			Editor_UI_Dict = ContentLoader.LoadMTextures(SpriteEditorMain.Instance.Content, "Editor/UI");
		}
	}
}
