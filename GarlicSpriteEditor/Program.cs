﻿using System;

namespace GarlicSpriteEditor
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new SpriteEditorMain(480,320,2,typeof(SpriteEditorMain).ToString(), false, Monocle.Monocle_Platform.PC))
                game.Run();
        }
    }
#endif
}
