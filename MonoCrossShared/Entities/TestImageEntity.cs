﻿using Monocle;
using System;
using System.Collections.Generic;
using System.Text;

namespace MonoCrossShared.Entities
{
    public class TestImageEntity : Entity
    {
		public TestImageEntity(MTexture tex) : base()
		{
			Add(new Image(tex));
		}
    }
}
