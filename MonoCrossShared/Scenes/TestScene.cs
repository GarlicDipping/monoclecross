﻿using System;
using System.Collections.Generic;
using System.Text;
using Monocle;

namespace MonoCrossShared.Scenes
{
	public class TestScene : Scene
	{
		public TestScene() : base() { }

		float x_vel = 1;
		public override void Begin()
		{
			base.Begin();
			Add(new EverythingRenderer());
		}

		public override void Update()
		{
			base.Update();
			List<Entity> e_list = GetEntitiesByTagMask(1);
			foreach(Entity e in e_list)
			{
				e.Position.X += x_vel;
				if(e.Position.X >= 310)
				{
					x_vel *= -1;
				}
				else if(e.Position.X <= 0)
				{
					x_vel *= -1;
				}
			}
		}

		public override void Render()
		{
			base.Render();
		}
	}
}
