using Monocle;
using MonoCrossShared.Entities;
using MonoCrossShared.Scenes;

namespace MonoCrossShared
{
	/// <summary>
	/// This is the main type for your game.
	/// </summary>
	public class EngineTest : Engine
	{				
		public EngineTest(int width, int height, int windowedScale, string windowTitle, bool fullscreen, Monocle_Platform platform) 
			: base(width, height, windowedScale, windowTitle, fullscreen, platform)
		{
			
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			// TODO: Add your initialization logic here
			//Initialize -> LoadContent -> LoadScene
			base.Initialize();
			LoadScene();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			base.LoadContent();
		}

		void LoadScene()
		{
			MTexture tex = new MTexture("Idle");
			TestImageEntity test_entity = new TestImageEntity(tex);
			test_entity.Tag = 1;

			Scene currentScene = new TestScene();
			currentScene.Add(test_entity);
			Scene = currentScene;
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// game-specific content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
			base.UnloadContent();
		}
	}
}
