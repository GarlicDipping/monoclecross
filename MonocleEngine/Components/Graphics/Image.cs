﻿using Microsoft.Xna.Framework;

namespace Monocle
{
    public class Image : GraphicsComponent
    {
        public MTexture Texture;

        public Image(MTexture texture)
            : base(false)
        {
            Texture = texture;
        }

        internal Image(MTexture texture, bool active)
            : base(active)
        {
            Texture = texture;
        }

        public override void Render()
        {
            if (Texture != null)
                Texture.Draw(RenderPosition, Origin, Color, Scale, Rotation, Effects);
        }

        public virtual float Width
        {
            get { return Texture.Width; }
        }

        public virtual float Height
        {
            get { return Texture.Height; }
        }

		public Vector2 TextureSize
		{
			get { return new Vector2(Width, Height); }
		}

        public void CenterOrigin()
        {
            Origin.X = Width / 2f;
            Origin.Y = Height / 2f;
        }

        public void JustifyOrigin(Vector2 at)
        {
            Origin.X = Width * at.X;
            Origin.Y = Height * at.Y;
        }

        public void JustifyOrigin(float x, float y)
        {
            Origin.X = Width * x;
            Origin.Y = Height * y;
        }

		void setScale(Vector2 targetSizePx)
		{
			Scale = new Vector2(targetSizePx.X / Texture.Width, targetSizePx.Y / Texture.Height);
		}

		public void setSize(float W, float H)
		{
			setScale(new Vector2(W, H));
		}

		public void setSize(Vector2 size)
		{
			setScale(size);
		}
	}
}
