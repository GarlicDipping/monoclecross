﻿using System;
using MonoCrossShared;

namespace MonoCrossTest
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
			using (var game = new EngineTest(480, 320, 2, "Hi", false, Monocle.Monocle_Platform.PC))
				game.Run();
        }
    }
#endif
}
